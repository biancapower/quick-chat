import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="messages"
export default class extends Controller {
  static targets = ["container", "input_form"]

  connect() {
    console.log(this.containerTarget)
    this.resetScroll();

    this.containerTarget.addEventListener("DOMNodeInserted", () => {
      this.resetScroll();
    });
  }

  resetScroll() {
    this.input_formTarget.reset(); //FIXME: clears the form field for both users. Should only clear for the user who sent the message.
    const scrollHeight = this.containerTarget.scrollHeight;
    this.containerTarget.scrollTo(0, scrollHeight);
  }

  disconnect() {
    console.log("MessagesController disconnected")
  }
}
