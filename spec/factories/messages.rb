FactoryBot.define do
  factory :message do
    body { "MyString" }
    user { nil }
    room { nil }
  end
end
